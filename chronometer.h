#ifndef CHRONOMETER_H
#define CHRONOMETER_H

#include <QObject>
#include <QString>
#include <QTimer>
#include <QDebug>
#include <ctime>
#include <chrono>
#include <vector>
#include <utility>

class Chronometer : public QObject
{
    Q_OBJECT

public:
    explicit Chronometer(QObject* = nullptr);
    explicit Chronometer(bool, int = 1, QObject* = nullptr);
    virtual ~Chronometer() {}

    virtual std::chrono::duration<double> elapsed();

signals:
    void stateChanged(QString);
    void newElapsedTime(QString);
    void newLap(int, QString, QString);
    void disableStart(bool);
    void disableStopLap(bool);
    void disableReset(bool);

public slots:
    virtual void start();
    virtual void stop();
    virtual void reset();
    virtual void updateElapsed();
    virtual void lap();

private:
    std::chrono::time_point<std::chrono::steady_clock> m_beginning;
    std::chrono::time_point<std::chrono::steady_clock> m_end;
    std::chrono::duration<double> m_totalElapsed;
    QTimer m_updateTimer;
    bool m_isRunning;
    bool m_hasBeenReset;
    int m_timerInterval;

public:
    std::vector<std::chrono::duration<double>> m_laps;

};

#endif // CHRONOMETER_H
