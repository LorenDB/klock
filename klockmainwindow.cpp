#include "klockmainwindow.h"

KlockMainWindow::KlockMainWindow(QWidget *parent)
    : QWidget(parent),
      m_timerBox{ new QGroupBox{ QObject::tr("&Timer") } },
      m_timerStart{ new QPushButton{ QObject::tr("Start") } },
      m_timerStop{ new QPushButton{ QObject::tr("Cancel") } },
      m_timerTime{ new QSpinBox },
      m_timerRepeat{ new QCheckBox{ QObject::tr("Repeat") } },
      m_timeLeft{ new QLabel{ "" } },
      m_progressTimeLeft{ new QProgressBar },
      m_chronoBox{ new QGroupBox{ QObject::tr("&Chronometer") } },
      m_chronoStart{ new QPushButton{ QObject::tr("Start") } },
      m_chronoStop{ new QPushButton{ QObject::tr("Stop") } },
      m_chronoReset{ new QPushButton{ QObject::tr("Reset") } },
      m_chronoLap{ new QPushButton{ QObject::tr("Lap") } },
      m_chronoTime{ new QLabel{ QObject::tr("0.000000 seconds") } },
      m_chronoLapTable{ new QTableWidget{ 1, 2 } },
      m_chronoLapTableHeaders{ "Lap", "Split" },
      m_mainWindowLayout{ new QGridLayout{ this } }
{
    // misc configuration
    m_timerTime->setSuffix(QObject::tr(" seconds"));
    m_chronoLapTable->setHorizontalHeaderLabels(m_chronoLapTableHeaders);
    m_chronoLapTable->setRowCount(0);
    m_progressTimeLeft->setMinimum(0);
    m_progressTimeLeft->setTextVisible(false);

    // disabled at startup
    m_timerStop->setDisabled(true);
    m_chronoStop->setDisabled(true);
    m_chronoLap->setDisabled(true);
    m_chronoReset->setDisabled(true);

    // set up the timer group layout
    m_timerLayout = new QGridLayout(m_timerBox);
    m_timerLayout->addWidget(m_timerStart, 1, 1);
    m_timerLayout->addWidget(m_timerTime, 1, 2);
    m_timerLayout->addWidget(m_timerStop, 2, 1);
    m_timerLayout->addWidget(m_timerRepeat, 2, 2);
    m_timerLayout->addWidget(m_timeLeft, 3, 1);
    m_timerLayout->addWidget(m_progressTimeLeft, 4, 1, 1, 2);

    // set up the chronometer group layout
    m_chronoLayout = new QGridLayout(m_chronoBox);
    m_chronoLayout->addWidget(m_chronoStart, 1, 1);
    m_chronoLayout->addWidget(m_chronoReset, 1, 2);
    m_chronoLayout->addWidget(m_chronoStop, 2, 1);
    m_chronoLayout->addWidget(m_chronoLap, 2, 2);
    m_chronoLayout->addWidget(m_chronoTime, 3, 1, 1, 2);
    m_chronoLayout->addWidget(m_chronoLapTable, 4, 1, 6, 2);

    // set up main window layout
    m_mainWindowLayout->addWidget(m_timerBox, 1, 1);
    m_mainWindowLayout->addWidget(m_chronoBox, 1, 2);
}

void KlockMainWindow::setupConnections(KlockTimer *timer, Chronometer *chrono)
{
    QObject::connect(m_timerStart, SIGNAL(clicked()), timer, SLOT(start()));
    QObject::connect(timer, SIGNAL(disableStart(bool)), m_timerStart, SLOT(setDisabled(bool)));
    QObject::connect(m_timerStop, SIGNAL(clicked()), timer, SLOT(stop()));
    QObject::connect(timer, SIGNAL(disableStop(bool)), m_timerStop, SLOT(setDisabled(bool)));
    QObject::connect(m_timerTime, SIGNAL(valueChanged(int)), timer, SLOT(updateTime(int)));
    QObject::connect(m_timerRepeat, SIGNAL(stateChanged(int)), timer, SLOT(setRepeatState(int)));
    QObject::connect(timer, SIGNAL(timeout()), timer, SLOT(timerUpNotify()));
    QObject::connect(timer, SIGNAL(updateTimeLeft(QString)), m_timeLeft, SLOT(setText(QString)));
    QObject::connect(timer, SIGNAL(updateTimeLeftProgressBar(int)), m_progressTimeLeft, SLOT(setValue(int)));
    QObject::connect(timer, SIGNAL(newDuration(int)), m_progressTimeLeft, SLOT(setMaximum(int)));

    QObject::connect(m_chronoStart, SIGNAL(clicked()), chrono, SLOT(start()));
    QObject::connect(chrono, SIGNAL(disableStart(bool)), m_chronoStart, SLOT(setDisabled(bool)));
    QObject::connect(m_chronoStop, SIGNAL(clicked()), chrono, SLOT(stop()));
    QObject::connect(chrono, SIGNAL(disableStopLap(bool)), m_chronoStop, SLOT(setDisabled(bool)));
    QObject::connect(m_chronoReset, SIGNAL(clicked()), chrono, SLOT(reset()));
    QObject::connect(chrono, SIGNAL(disableReset(bool)), m_chronoReset, SLOT(setDisabled(bool)));
    QObject::connect(m_chronoLap, SIGNAL(clicked()), chrono, SLOT(lap()));
    QObject::connect(chrono, SIGNAL(disableStopLap(bool)), m_chronoLap, SLOT(setDisabled(bool)));
    QObject::connect(chrono, SIGNAL(stateChanged(QString)), m_chronoTime, SLOT(setText(QString)));
    QObject::connect(chrono, SIGNAL(newElapsedTime(QString)), m_chronoTime, SLOT(setText(QString)));
    QObject::connect(chrono, SIGNAL(newLap(int, QString, QString)), this, SLOT(chronoCreateTableEntry(int, QString, QString)));
}

void KlockMainWindow::chronoCreateTableEntry(int lapNum, QString lapString, QString splitString)
{
    m_chronoLapTable->setRowCount(lapNum);
    if (lapNum == 0)
        return;

    // remember that QTableWidget columns and rows are zero-based!
    m_chronoLapTable->setItem(lapNum - 1, 0, new QTableWidgetItem{ lapString });
    m_chronoLapTable->setItem(lapNum - 1, 1, new QTableWidgetItem{ splitString });
    // and make sure it's completely visible
    m_chronoLapTable->resizeColumnsToContents();
}
