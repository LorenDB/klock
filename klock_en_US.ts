<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>:</name>
    <message>
        <location filename="chronometer.cpp" line="79"/>
        <source></source>
        <comment>This and the below &quot; s&quot; are simply marking seconds, change accordingly</comment>
        <translatorcomment>This and the below &quot; s&quot; are simply marking seconds, change accordingly</translatorcomment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Chronometer</name>
    <message>
        <location filename="chronometer.cpp" line="27"/>
        <source>Chronometer started</source>
        <translation>Chronometer started</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="41"/>
        <source>Chronometer stopped</source>
        <translation>Chronometer stopped</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="54"/>
        <source>Chronometer reset</source>
        <translation>Chronometer reset</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="79"/>
        <location filename="chronometer.cpp" line="83"/>
        <source> s</source>
        <translation> s</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="89"/>
        <source>Lap </source>
        <translation>Lap </translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="93"/>
        <source>New lap: </source>
        <translation>New lap: </translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="107"/>
        <source> seconds</source>
        <translation> seconds</translation>
    </message>
</context>
<context>
    <name>KlockMainWindow</name>
    <message>
        <source>Start timer</source>
        <oldsource>Set a timer</oldsource>
        <translation type="vanished">Start timer</translation>
    </message>
    <message>
        <source> seconds</source>
        <translation type="vanished"> seconds</translation>
    </message>
    <message>
        <source>Welcome to Klock, the time utility application!</source>
        <translation type="vanished">Welcome to Klock, the time utility application!</translation>
    </message>
    <message>
        <source>Klock</source>
        <translation type="vanished">Klock</translation>
    </message>
    <message>
        <source>Timer ended</source>
        <translation type="vanished">Timer ended</translation>
    </message>
    <message>
        <source>Timer timed out</source>
        <translation type="vanished">Timer timed out</translation>
    </message>
    <message>
        <source>Time up!</source>
        <translation type="vanished">Time up!</translation>
    </message>
</context>
<context>
    <name>KlockTimer</name>
    <message>
        <source>Timer start triggered (</source>
        <translation type="vanished">Timer start triggered (</translation>
    </message>
    <message>
        <source> milliseconds)</source>
        <translation type="vanished"> milliseconds)</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="22"/>
        <source>Timer started</source>
        <translation>Timer started</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="30"/>
        <source>Timer stopped</source>
        <translation>Timer stopped</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="36"/>
        <source>New timer value (in milliseconds): </source>
        <translation>New timer value (in milliseconds): </translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="45"/>
        <source>Error: invalid state emitted by checkbox</source>
        <translation>Error: invalid state emitted by checkbox</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="46"/>
        <source>isSingleShot(): </source>
        <translation>isSingleShot(): </translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="52"/>
        <source>Time up!</source>
        <translation>Time up!</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="57"/>
        <source>Timer timed out</source>
        <translation>Timer timed out</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="65"/>
        <source> seconds left</source>
        <translation> seconds left</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Welcome to Klock, the time utility application!</source>
        <translation type="vanished">Welcome to Klock, the time utility application!</translation>
    </message>
    <message>
        <source>Start timer</source>
        <translation type="vanished">Start timer</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="5"/>
        <source>&amp;Timer</source>
        <translation>&amp;Timer</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="6"/>
        <location filename="klockmainwindow.cpp" line="12"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="7"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="11"/>
        <source>&amp;Chronometer</source>
        <translation>&amp;Chronometer</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="13"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="14"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="15"/>
        <source>Lap</source>
        <translation>Lap</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="16"/>
        <source>0.000000 seconds</source>
        <translation>0.000000 seconds</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="22"/>
        <source> seconds</source>
        <translation> seconds</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="9"/>
        <source>Repeat</source>
        <translation>Repeat</translation>
    </message>
    <message>
        <source>Start chronometer</source>
        <translation type="vanished">Start chronometer</translation>
    </message>
    <message>
        <source>Stop chronometer</source>
        <translation type="vanished">Stop chronometer</translation>
    </message>
    <message>
        <source>Not yet implemented</source>
        <translation type="vanished">Not yet implemented</translation>
    </message>
    <message>
        <location filename="main.cpp" line="17"/>
        <source>Klock</source>
        <translation>Klock</translation>
    </message>
</context>
</TS>
