#include <QObject>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>
#include "klocktimer.h"

KlockTimer::KlockTimer(QObject *parent)
    : QTimer(parent), m_updateTimeLeft{ this }
{
    QTimer::setSingleShot(true);
    m_updateTimeLeft.setSingleShot(false);
    m_updateTimeLeft.setInterval(1);
    connect(&m_updateTimeLeft, SIGNAL(timeout()), this, SLOT(createTimeLeftStr()));
}

void KlockTimer::start()
{
    QTimer::start();
    m_updateTimeLeft.start();
    emit disableStart(true);
    emit disableStop(false);
    qDebug() << tr("Timer started");
}

void KlockTimer::stop()
{
    QTimer::stop();
    emit disableStart(false);
    emit disableStop(true);
    qDebug() << tr("Timer stopped");
}

void KlockTimer::updateTime(int seconds)
{
    QTimer::setInterval(seconds * 1000);
    emit newDuration(interval());
    qDebug() << tr("New timer value (in milliseconds): ") << seconds * 1000;
}

void KlockTimer::setRepeatState(int state)
{
    // We're setting the singleShot property, but the check box controls the
    // repeat state; hence the negation
    if (state == 2) setSingleShot(false);
    else if (state == 0) setSingleShot(true);
    else qDebug() << tr("Error: invalid state emitted by checkbox");
    qDebug() << tr("isSingleShot(): ") << QTimer::isSingleShot();
}

void KlockTimer::timerUpNotify()
{
    QMessageBox timeUp;
    timeUp.setText(tr("Time up!"));
    timeUp.exec();
    emit disableStart(false);
    emit disableStop(true);
    emit updateTimeLeft(""); // TODO: figure out why this doesn't work as expected
    qDebug() << tr("Timer timed out");
}

void KlockTimer::createTimeLeftStr()
{
    QString s;
    // add one to round decimals up
    s.setNum((remainingTime() / 1000) + 1);
    s.append(tr(" seconds left"));
    emit updateTimeLeft(s);
    emit updateTimeLeftProgressBar(remainingTime());
}
