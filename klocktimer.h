#ifndef TIMER_H
#define TIMER_H

#include <QTimer>
#include <QDebug>

class KlockTimer : public QTimer
{
    Q_OBJECT

public:
    KlockTimer(QObject * = nullptr);

signals:
    void disableStart(bool);
    void disableStop(bool);
    void updateTimeLeft(QString);
    void updateTimeLeftProgressBar(int);
    void newDuration(int);

public slots:
    virtual void start();
    virtual void stop();
    virtual void updateTime(int);
    virtual void setRepeatState(int);
    virtual void timerUpNotify();
    virtual void createTimeLeftStr();

private:
    int m_ms;
    QTimer m_updateTimeLeft;
};

#endif // TIMER_H
