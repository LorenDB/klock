# Klock
Klock is a Qt-based clock utility application. The aim of this project is:
- to help me (@LorenDB) learn Qt
- to fill a missing gap in the Linux software universe
- to eventually get integrated into [KDE](https://www.kde.org)

## Installation
Clone the repo, open the project in Qt Creator, and build and run the program.

To use the last official release, run `git checkout release` in the local repo.

# Warning!
This software is in an alpha state. Functionality may be limited.

