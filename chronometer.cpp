#include "chronometer.h"

#include <QString>
#include <QDebug>

Chronometer::Chronometer(QObject *parent)
    : QObject(parent), m_totalElapsed{ 0 },
      m_isRunning{ false }, m_timerInterval{ 1 }
{
    m_updateTimer.setSingleShot(false);
    connect(&m_updateTimer, SIGNAL(timeout()), this, SLOT(updateElapsed()));
}

Chronometer::Chronometer(bool startNow, int interval, QObject *parent)
    : QObject(parent), m_totalElapsed{ 0 }, m_isRunning{ startNow }, m_timerInterval{ interval }
{
}

void Chronometer::start()
{
    m_beginning = std::chrono::steady_clock::now();
    m_isRunning = true;
    m_updateTimer.start(m_timerInterval);
    emit disableStart(true);
    emit disableStopLap(false);
    emit disableReset(false);
    qDebug() << tr("Chronometer started");
}

void Chronometer::stop()
{
    if (!m_isRunning)
        return;
    m_end = std::chrono::steady_clock::now();
    m_updateTimer.stop();
    m_isRunning = false;
    m_totalElapsed += m_end - m_beginning;
    emit disableStart(false);
    emit disableStopLap(true);
    updateElapsed();
    qDebug() << tr("Chronometer stopped");
}

void Chronometer::reset()
{
    this->stop();
    m_totalElapsed -= m_totalElapsed; // KLUDGE: there's no simple way to set a std::chrono::duration to zero so I just did this instead
    m_beginning = std::chrono::steady_clock::now();
    for (int i = m_laps.size(); i > 0; --i) // KLUDGE: the size of m_laps decreases every pop_back(), so it's not reliable
        m_laps.pop_back();
    updateElapsed();
    emit disableReset(true);
    emit newLap(0, "", ""); // zero meaning we've reset
    qDebug() << tr("Chronometer reset");
}

void Chronometer::lap()
{
    // for simplicity, don't allow laps while stopped
    if (!m_isRunning)
        return;

    // so all calculations use the same value
    auto time = elapsed();

    std::chrono::duration<double> totalLapTime; // this calculates the total amount of time lapped
    for (auto &x: m_laps) // and removes it from the total elapsed to get what hasn't been lapped yet
        totalLapTime += x;

    // first lap, starts from beginning
    if (m_laps.size() == 0)
            m_laps.push_back(time);

    else // start from beginning of last lap
        m_laps.push_back(time - totalLapTime);

    QString lapNum, lapString, splitString; // lapString is the whole thing put together
    lapString.setNum(m_laps.back().count(), 'f');
    lapString.append(tr(" s")); // TRANSLATOR: This and the below " s" are simply marking seconds, change accordingly
    // add this lap to total time
    totalLapTime += m_laps.back();
    splitString.setNum(totalLapTime.count(), 'f');
    splitString.append(tr(" s"));

    // emit now so the string doesn't have the "Lap x: " prepended
    emit newLap(m_laps.size(), lapString, splitString);

    lapNum.setNum(m_laps.size());
    lapNum.prepend(tr("Lap "));
    lapNum.append(": "); // TRANSLATOR: punctuation, just copy-paste

    lapString.prepend(lapNum);
    qDebug() << tr("New lap: ") << lapString;
}

std::chrono::duration<double> Chronometer::elapsed()
{
    std::chrono::duration<double> elapsedTime;
    m_isRunning ? elapsedTime = (std::chrono::steady_clock::now() - m_beginning) + m_totalElapsed : elapsedTime = m_totalElapsed;
    return elapsedTime;
}

void Chronometer::updateElapsed()
{
    QString s;
    s.setNum(elapsed().count(), 'f');
    s.append(tr(" seconds"));
    emit newElapsedTime(s);
}
