#include <QApplication>
#include <QDebug>
#include "klockmainwindow.h"
#include "klocktimer.h"
#include "chronometer.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    KlockMainWindow mainWindow;

    KlockTimer *timer = new KlockTimer{ &mainWindow };
    Chronometer *chrono = new Chronometer{ &mainWindow };

    mainWindow.setupConnections(timer, chrono);
    mainWindow.setWindowTitle(QObject::tr("Klock"));
    mainWindow.show();

    return a.exec();
}
