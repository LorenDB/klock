# CHANGELOG.md
This is the changelog for Klock, the Linux time utility program.

# Current version
=================
## 0.0.4 - 2020-7-27
### What's new
 - Timer has progress bar
 - Chronometer displays lap and split times in a table
 - Reset now stops the chronometer
 - The reset button disables appropriately
### Known bugs
 - The timer display of time left doesn't clear itself when the timer finishes

# Older versions
================
## 0.0.3 - 2020-7-24
### What's new
 - Timer displays time left
 - Chronometer now supports laps
 - Buttons are disabled appropriately
### Known bugs
 - The timer display of time left doesn't clear itself when the timer finishes

## 0.0.2 - 2020-7-22
### What's new
 - Chronometer
    - Start, stop, and reset
 - Timer stop
 - Grouped timer and chronometer tools appropriately
 - en_US and es_MX translations for most of program

## 0.0.1 - 2020-7-21
### What's new
 - Timer
