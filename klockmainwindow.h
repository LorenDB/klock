#ifndef KLOCKMAINWINDOW_H
#define KLOCKMAINWINDOW_H

#include <QWidget>
#include <QMessageBox>
#include <QGridLayout>
#include <QSpinBox>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>
#include <QGroupBox>
#include <QHeaderView>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QStringList>
#include <QProgressBar>
#include <QDebug>
#include <ctime>
#include <chrono>
#include "klocktimer.h"
#include "chronometer.h"

class KlockMainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit KlockMainWindow(QWidget *parent = nullptr);
    virtual ~KlockMainWindow() {}
    virtual void setupConnections(KlockTimer *, Chronometer *);

    QGridLayout *m_timerLayout;
    QGridLayout *m_chronoLayout;

    QGroupBox *m_timerBox;
    QPushButton *m_timerStart;
    QPushButton *m_timerStop;
    QSpinBox *m_timerTime;
    QCheckBox *m_timerRepeat;
    QLabel *m_timeLeft;
    QProgressBar *m_progressTimeLeft;

    QGroupBox *m_chronoBox;
    QPushButton *m_chronoStart;
    QPushButton *m_chronoStop;
    QPushButton *m_chronoReset;
    QPushButton *m_chronoLap;
    QLabel *m_chronoTime;
    QLabel *m_chronoLapLabel;

    QTableWidget *m_chronoLapTable;
    QStringList m_chronoLapTableHeaders;

    QGridLayout *m_mainWindowLayout;

signals:

public slots:
    void chronoCreateTableEntry(int, QString, QString);

private:
    int numTableColumns;
};

#endif // KLOCKMAINWINDOW_H
