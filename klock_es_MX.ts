<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>Chronometer</name>
    <message>
        <location filename="chronometer.cpp" line="22"/>
        <source>Chronometer started</source>
        <translation>Cronómetro comenzó</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="33"/>
        <source>Chronometer stopped</source>
        <translation>Cronómetro cancelaste</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="37"/>
        <source>Chronometer reset</source>
        <translation>Reinicio cronómetro</translation>
    </message>
    <message>
        <location filename="chronometer.cpp" line="56"/>
        <source> seconds</source>
        <translation> segundos</translation>
    </message>
</context>
<context>
    <name>KlockMainWindow</name>
    <message>
        <location filename="klockmainwindow.cpp" line="9"/>
        <source>Timer timed out</source>
        <oldsource>Timer ended</oldsource>
        <translation>Temporizador ha expirado</translation>
    </message>
    <message>
        <location filename="klockmainwindow.cpp" line="11"/>
        <source>Time up!</source>
        <translation>¡El tiempo ha terminado!</translation>
    </message>
</context>
<context>
    <name>KlockTimer</name>
    <message>
        <location filename="klocktimer.cpp" line="14"/>
        <source>Timer start triggered (</source>
        <translation>Temporizador comenzó (</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="14"/>
        <source> milliseconds)</source>
        <translation>milisegundos)</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="20"/>
        <source>New timer value (in milliseconds): </source>
        <translation>Nuevo valor del temporizador (en milisegundos): </translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="29"/>
        <source>Error: invalid state emitted by checkbox</source>
        <translation>Error: estado no válido emitido por casilla</translation>
    </message>
    <message>
        <location filename="klocktimer.cpp" line="30"/>
        <source>isSingleShot(): </source>
        <translation>isSingleShot(): </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="21"/>
        <source>Start timer</source>
        <translation>Iniciar un temporizador</translation>
    </message>
    <message>
        <location filename="main.cpp" line="23"/>
        <source> seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="main.cpp" line="20"/>
        <source>Welcome to Klock, the time utility application!</source>
        <translation>Bienvenido a Klock, la aplicación de utilidad tiempo!</translation>
    </message>
    <message>
        <location filename="main.cpp" line="24"/>
        <source>Repeat</source>
        <translation>repitan</translation>
    </message>
    <message>
        <location filename="main.cpp" line="25"/>
        <source>Start chronometer</source>
        <translation>Inicio cronómetro</translation>
    </message>
    <message>
        <location filename="main.cpp" line="26"/>
        <source>Stop chronometer</source>
        <translation>Detener cronómetro</translation>
    </message>
    <message>
        <source>Not yet implemented</source>
        <translation type="vanished">Aun no implementado</translation>
    </message>
    <message>
        <location filename="main.cpp" line="47"/>
        <source>Klock</source>
        <translation>Klock</translation>
    </message>
</context>
</TS>
